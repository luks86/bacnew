import { Component, ViewChild  } from '@angular/core';
import { Platform, App, AlertController, NavController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage:any = HomePage;
  @ViewChild('myNav') nav: NavController

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, app: App,
    alertCtrl: AlertController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    
    /*platform.registerBackButtonAction(() => {
      
      let navaux = app.getActiveNavs()[0];
      let activeView = navaux.getActive();     
      console.log(activeView);         
      // Checks if can go back before show up the alert
      if(1) {
          if (this.nav.canGoBack()){
              this.nav.pop();
          } else {
              const alert = alertCtrl.create({
                  title: 'Salir de BAC',
                  message: 'La aplicación se cerrará',
                  buttons: [{
                      text: 'Cancelar',
                      role: 'cancel',
                      handler: () => {
                        this.nav.setRoot('HomePage');
                        console.log('** BAC esta activo! **');
                      }
                  },{
                      text: 'Saliendo...',
                      handler: () => {
                        // this.logout();
                        platform.exitApp();
                      }
                  }]
              });
              alert.present();
          }
      }
  });*/

  }
}

